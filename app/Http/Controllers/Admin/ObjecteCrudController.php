<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ObjecteRequest as StoreRequest;
use App\Http\Requests\ObjecteRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class ObjecteCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ObjecteCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Objecte');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/objecte');
        $this->crud->setEntityNameStrings('objecte', 'objectes');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
	// $this->crud->setFromDb();
	$this->crud->setColumns(['Efecto', 'Imatge(Ruta)','idStats']);
	$this->crud->addField([
        'name' => 'Efecto',
        'type' => 'text',
        'label' => "Efecto"
      ]);
	$this->crud->addField([
        'name' => 'Imatge(Ruta)',
        'type' => 'text',
        'label' => "Ruta Imatge"
      ]);
	$this->crud->addField([
        'name' => 'idStats',
        'type' => 'integer',
        'label' => "ID Stat"
      ]);

        // add asterisk for fields that are required in ObjecteRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
