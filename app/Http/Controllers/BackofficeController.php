<?php

namespace App\Http\Controllers;

use App\Enemic;
use App\Nivell;
use App\Objecte;
use App\Personatge;
use App\Puntuacio;
use App\StatsBase;
use App\StatsEnemic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BackofficeController extends Controller
{
    function removeElement($array,$value) {
        if (($key = array_search($value, $array)) !== false) {
          unset($array[$key]);
        }
       return $array;
     }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('backoffice');
    }
    /*
            OBJECTES
    */
    public function objectes()
    {
        $objectes = DB::table('Objectes')->get();
        $data["objectes"] = $objectes;
        //dd($objectes);
        return view('objectes', $data);
    }
    public function getObjectes()
    {
        $objectes = DB::table('Objectes')->get();
        //dd($objectes);
        return response()->json($objectes);
    }


    /*
            NIVELL
    */
    public function nivell()
    {
        $nivells = DB::table('Nivell')->get();
        $data["nivells"] = $nivells;
        //dd($nivells);
        return view('nivell', $data);
    }
    public function getNivells()
    {
        $nivells = DB::table('Nivell')->get();
        //dd($nivells);
        return response()->json($nivells);
    }

    /*
            ENEMICS
    */
    public function enemics()
    {
        $enemics = DB::table('Enemic')->get();
        $data["enemics"] = $enemics;
        //dd($enemics);
        return view('enemics', $data);
    }
    public function getEnemics()
    {
        $enemics = DB::table('Enemic')->get();
        //dd($enemics);
        return response()->json($enemics);
    }

    /*
            PERSONATGES
    */
    public function personatges()
    {
        $personatges = DB::table('Personatge')->get();
        $data["personatges"] = $personatges;
        //dd($personatges);
        return view('personatges', $data);
    }
    public function getPersonatges()
    {
        $personatges = DB::table('Personatge')->get();
        //dd($personatges);
        return response()->json($personatges);
    }

    /*
            STATS BASE
    */
    public function statsbase()
    {
        $statsbase = DB::table('StatsBase')->get();
        $data["statsbase"] = $statsbase;
        //dd($statsbase);
        return view('statsbase', $data);
    }
    public function getStatsBase()
    {
        $statsbase = DB::table('StatsBase')->get();
        //dd($statsbase);
        return response()->json($statsbase);
    }

    /*
            STATS ENEMIC
    */
    public function statsenemics()
    {
        $statsenemics = DB::table('StatsEnemic')->get();
        $data["statsenemics"] = $statsenemics;
        //dd($statsenemics);
        return view('statsenemics', $data);
    }
    public function getStatsEnemics()
    {
        $statsenemics = DB::table('StatsEnemic')->get();
        //dd($statsenemics);
        return response()->json($statsenemics);
    }

    /*
            PUNTUACIO
    */
    public function puntuacio()
    {
        $puntuacions = DB::select('SELECT idPuntuacio,Nom,Puntuacio,Tipus FROM `Puntuacio` p INNER JOIN `Personatge` pe ON p.idPNG = pe.idPersonatge ') ;
        $data["puntuacions"] = $puntuacions;
        //dd($puntuacions);
        return view('puntuacio', $data);
    }
    public function getPuntuacions()
    {
        $puntuacions = DB::select('SELECT idPuntuacio,Nom,Puntuacio,Tipus FROM `Puntuacio` p INNER JOIN `Personatge` pe ON p.idPNG = pe.idPersonatge ') ;
        //dd($puntuacions);
        return response()->json($puntuacions);
    }

    public function edit($taula, $id){
        $resQuery = [];
        switch ($taula) {
            case 'Objectes':
                $objecteEdicio = DB::table('Objectes')->where('idObjecte','=', $id)->get();
                break;
            case 'StatsBase':
                $objecteEdicio = DB::table('StatsBase')->where('idBase','=', $id)->get();
                break;
            case 'Personatges':
                $objecteEdicio = DB::table('Personatge')->where('idPersonatge','=', $id)->get();
                break;
            case 'StatsEnemics':
                $objecteEdicio = DB::table('StatsEnemic')->where('idEnemic', $id)->get();
                break;
            case 'Enemics':
                $objecteEdicio = DB::table('Enemic')->where('idEnemic','=', $id)->get();
                break;
            case 'Nivells':
                $objecteEdicio = DB::table('Nivell')->where('idNivell','=', $id)->get();
                break;
            case 'Puntuacions':
                $objecteEdicio = DB::table('Puntuacio')->where('idPuntuacio','=', $id)->get();
                break;
            default:
                $objecteEdicio = [
                    'result' => 'error',
                    'msg' => 'taula no trobada'
                ];
                break;
        }
        return response()->json($objecteEdicio);
    }
    public function editobj(Request $request){
        
        $taula = $request->input('tabla');
        $objecteAEditar = $request->input('obj');
        //dd($objecteAEditar);
        $resQuery = '';
        switch ($taula) {
            case 'Objectes':
                $idObjecteAEditar = $objecteAEditar['idObjecte'];
                $objecteAEditar = $this->removeElement($objecteAEditar,$idObjecteAEditar);
                $resQuery = DB::table('Objectes')->where('idObjecte','=', $idObjecteAEditar)
                ->update($objecteAEditar);
                break;
            case 'StatsBase':
                $idObjecteAEditar = $objecteAEditar['idBase'];
                $objecteAEditar = $this->removeElement($objecteAEditar,$idObjecteAEditar);
                $resQuery = DB::table('StatsBase')->where('idBase','=', $idObjecteAEditar)
                ->update($objecteAEditar);
                break;
            case 'Personatges':
                $idObjecteAEditar = $objecteAEditar['idPersonatge'];
                $objecteAEditar = $this->removeElement($objecteAEditar,$idObjecteAEditar);
                $resQuery = DB::table('Personatge')->where('idPersonatge','=', $idObjecteAEditar)
                ->update($objecteAEditar);
                break;
            case 'StatsEnemics':
                $idObjecteAEditar = $objecteAEditar['idEnemic'];
                $objecteAEditar = $this->removeElement($objecteAEditar,$idObjecteAEditar);
                $resQuery = DB::table('StatsEnemic')->where('idEnemic', $idObjecteAEditar)
                ->update($objecteAEditar);
                break;
            case 'Enemics':
                $idObjecteAEditar = $objecteAEditar['idEnemic'];
                $objecteAEditar = $this->removeElement($objecteAEditar,$idObjecteAEditar);
                $resQuery = DB::table('Enemic')->where('idEnemic','=', $idObjecteAEditar)
                ->update($objecteAEditar);
                break;
            case 'Nivells':
                $idObjecteAEditar = $objecteAEditar['idNivell'];
                $objecteAEditar = $this->removeElement($objecteAEditar,$idObjecteAEditar);
                $resQuery = DB::table('Nivell')->where('idNivell','=', $idObjecteAEditar)
                ->update($objecteAEditar);
                break;
            case 'Puntuacions':
                $idObjecteAEditar = $objecteAEditar['idPuntuacio'];
                $objecteAEditar = $this->removeElement($objecteAEditar,$idObjecteAEditar);
                $resQuery = DB::table('Puntuacio')->where('idPuntuacio','=', $idObjecteAEditar)
                ->update($objecteAEditar);
                break;
            default:
                $resQuery = [
                    'result' => 'Error',
                    'msg' => 'Taula no trobada'
                ];
                break;
        }
        if($resQuery){
            $resQuery = [
                'result' => 'OK',
                'msg' => 'Edició satisfactòria'
            ];
        }else{
            $resQuery = [
                'result' => 'Error',
                'msg' => "No s'ha pogut editar"
            ];
        }
        return response()->json($resQuery);
    }

}
