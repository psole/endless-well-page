<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProjecteController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function inici()
    {
        return view('inici');
    }

    public function descarga()
    {
        return response()->download(storage_path('app/public/prueba.zip'));
    }

    public function faq()
    {
        return view('faq');
    }

    public function wiki()
    {
        return view('wiki');
    }

    public function contacto()
    {
        return view('contacto');
    }

    public function backoffice()
    {
        return view('backoffice');
    }
}
