<?php

namespace App\Http\Controllers;

use App\Enemic;
use App\Nivell;
use App\Objecte;
use App\Personatge;
use App\Puntuacio;
use App\StatsBase;
use App\StatsEnemic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WikiController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $data["enemics"] = $this->getEnemics();
        $data["nivells"] = $this->getNivells();
        $data["objectes"] = $this->getObjectes();
        $data["personatges"] = $this->getPersonatges();
        return view('wiki', $data);
    }

    public function getEntrada(Request $request){
        $taula = $request->input('entrada');
        $id = $request->input('idEntrada');
        switch ($taula) {
            case 'Objectes':
                $camp = "idObjecte";
                $entrada = DB::table($taula)
                ->join('StatsBase', ''.$taula.'.idStats', '=', 'StatsBase.idBase')
                ->where($camp,'=', $id)
                ->get();
                break;
            case 'Personatge':
                $camp = "idPersonatge";
                $entrada = DB::table($taula)
                ->join('StatsBase', ''.$taula.'.idStats', '=', 'StatsBase.idBase')
                ->where($camp,'=', $id)
                ->get();
                break;
            case 'Nivell':
                $camp = "idNivell";
                $entrada = DB::table($taula)->where($camp,'=', $id)->get();
                break;
            case 'Enemic':
                $camp = "idEnemic";
                $entrada = DB::table($taula)
                ->join('StatsEnemic', ''.$taula.'.idEnemic', '=', 'StatsEnemic.idEnemic')
                ->where('Enemic.'.$camp,'=', $id)
                ->get();
                break;
            default:
                # code...
                break;
        }
        return response()->json($entrada);
    }
    public function getObjectes()
    {
        $objectes = DB::table('Objectes')->get();
        //dd($objectes);
        return $objectes;
    }
    public function getNivells()
    {
        $nivells = DB::table('Nivell')->get();
        //dd($nivells);
        return $nivells;
    }
    public function getEnemics()
    {
        $enemics = DB::table('Enemic')->get();
        //dd($enemics);
        return $enemics;
    }
    public function getPersonatges()
    {
        $personatges = DB::table('Personatge')->get();
        //dd($personatges);
        return $personatges;
    }
}
