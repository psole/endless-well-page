<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Objecte extends Model
{
    protected $table = "Objecte";
    protected $fillable = [
	'Efecto','Imatge(Ruta)','idStats'
];
}
