var vueObj = new Vue({
    el: '#projectefinal',
    data: {
        url: '',
        urlajax: '',
        nameTableActive: '',
        itemsTableActive: [],
        idEdicio: '',
        objecteEdicio:'',
    },
    mounted: function () {
        var dir = window.location.href;
        if (dir.includes("backoffice")) {
            //console.log(dir);
            this.url = dir;
        }
    },
    methods: {
        getTable: function (active, event) {
            event.preventDefault();
            //console.log(active);
            if (this.nameTableActive == active) {
                this.nameTableActive = '';
                this.itemsTableActive = '';
                return false;
            }
            //console.log(this.nameTableActive);
            urlajax = this.url + '/get' + active;
            //console.log('URLAJAX: ' + urlajax);
            //return false;
            this.$http.get(urlajax)
                .then(response => {
                    this.itemsTableActive = response.body;
                    this.nameTableActive = active;
                    //console.log(active);
                    //console.log(response.body); 
                })
                .catch((err) => {
                    console.log(err);
                });
            return;
        },
        edit: function (idEdit) {
            //console.log(idEdit);
            this.idEdicio = idEdit;
            //console.log(this.nameTableActive);
            
            urlajax = this.url + '/' + this.nameTableActive + '/edit/' + this.idEdicio;
            //console.log('URLAJAX: ' + urlajax);
            //return false;
            this.$http.get(urlajax)
                .then(response => {
                    //console.log(response.body);
                    this.objecteEdicio = response.body[0];
                    //console.log(this.objecteEdicio);
                })
                .catch((err) => {
                    console.log(err);
                });
        },
        editobj: function(event){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            urlajax = this.url + '/editobj';
            //console.log(urlajax);
            var taula = this.nameTableActive;
            $.ajax({
                url: urlajax,
                type: "POST",
                data: {
                    tabla: this.nameTableActive,
                    obj: this.objecteEdicio
                },
                dataType: 'json',
                success: function (response) {
                    //console.log(response);
                    $('#modal-center').modal('toggle');
                    var reload = vueObj.nameTableActive;
                    vueObj.getTable(vueObj.nameTableActive, event);
                    vueObj.getTable(reload, event);
                    app.toast(response.msg);
                },
                error: function (response) {
                    app.toast(response.msg);
                }
            });
            
        },
        
    }
});
