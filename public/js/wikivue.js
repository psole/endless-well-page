var wikiVue = new Vue({
    el: '#wiki',
    data: {
        url: '',
        urlajax: '',
        urlimg:'',
        entrada: '',
        idEntrada:'',
        imgentrada:'',
        nomEntrada:'',
        nomEntradaCache:'',
        objEntrada:'',
        keysEntrada:'',
        valuesEntrada:'',
    },
    mounted: function () {
        var dir = window.location.href;
        if (dir.includes("wiki")) {
            //console.log(dir);
            this.url = dir;
            var str = "wiki";
            this.urlimg = dir.replace(str, "");
            this.urlimg = this.urlimg + 'storage/imagenes/';
            //console.log(this.urlimg);
        }
    },
    methods: {
        edit: function (idEdit) {
            //console.log(idEdit);
            this.idEdicio = idEdit;
            //console.log(this.nameTableActive);
            
            urlajax = this.url + '/' + this.nameTableActive + '/edit/' + this.idEdicio;
            //console.log('URLAJAX: ' + urlajax);
            //return false;
            this.$http.get(urlajax)
                .then(response => {
                    //console.log(response.body);
                    this.objecteEdicio = response.body[0];
                    //console.log(this.objecteEdicio);
                })
                .catch((err) => {
                    console.log(err);
                });
        },
        getEntrada: function(ent,event){
            if(this.nomEntrada == ent){
                this.nomEntrada = '';
                this.objEntrada = '';
                this.keysEntrada = '';
                this.valuesEntrada = '';
                return false;
            }
            this.nomEntrada = ent;
            ent = ent.split('_');
            this.entrada = ent[0];
            this.idEntrada = ent[1];
            switch (this.entrada) {
                case 'en':
                    this.entrada = 'Enemic';
                    break;
                case 'pe':
                    this.entrada = 'Personatge';
                    break;
                case 'ob':
                    this.entrada = 'Objectes';
                    break;
                case 'ni':
                    this.entrada = 'Nivell';
                    break;
                default:
                    console.log('No trobat');
                    return false;
                    break;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            urlajax = this.url + '/getentrada';
            //console.log(urlajax);
            $.ajax({
                url: urlajax,
                type: "POST",
                data: {
                    entrada: this.entrada,
                    idEntrada: this.idEntrada
                },
                dataType: 'json',
                success: function (response) {
                    console.log(response[0]);
                    wikiVue.objEntrada = response[0]; 
                    wikiVue.keysEntrada = Object.keys(response[0]);
                    wikiVue.valuesEntrada = Object.values(response[0]);
                },
                error: function (response) {
                    console.log(response);
                }
            });
            
        },
        
    }
});
