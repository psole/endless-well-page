@extends('layouts.nav')

@section('content') 
<br><br><br>
<div class="row min-h-fullscreen center-vh p-20 m-0 text-dark">
    <div class="col-12">
        <div class="card card-shadowed  px-50 py-30 w-400px mx-auto" style="max-width: 100%">
            {{--<div class="card">--}}
                <h5 class="text-uppercase">{{ __('Inicia sessió') }}</h5>

                {{--<div class="card-body">--}}
                    <form method="POST" action="{{ route('login') }}" class="form-type-material">
                        @csrf

                        <div class="form-group">
                            <div class="form-group">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" >
                                <label for="email">{{ __('Correu Electrònic') }}</label>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                <label for="password" >{{ __('Contrasenya') }}</label>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group flexbox flex-column flex-md-row">
                            <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="remember">
                                        {{ __("Recorda'm") }}
                                    </label>
                            </div>
                            @if (Route::has('password.request'))
                                    <a class="text-muted hover-primary fs-13 mt-2 mt-md-0" href="{{ route('password.request') }}">
                                        {{ __('Contrasenya Oblidada?') }}
                                    </a>
                            @endif
                        </div>

                        <div class="form-group">
                                <button type="submit" class="btn btn-bold btn-block btn-primary">
                                    {{ __('Login') }}
                                </button>
                        </div>
                    </form>
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
</div>
<br><br>
@endsection
