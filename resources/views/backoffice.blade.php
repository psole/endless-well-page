@extends('layouts.backend')

@section('content')

<br>
<div id="projectefinal" class="col-12 ">
    <div class="card">
        <template v-if="!nameTableActive">
            <h5 class="card-title"><strong>Backoffice</strong></h5>
        </template>
        <template v-else>
            <h5 class="card-title"><strong>@{{nameTableActive}}</strong></h5>
        </template>
        <div class="card-body">

            <div class="nav-tabs-left">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-tabs-warning">
                    <li class="nav-item">
                        <span class="nav-link pointer hover-dark" data-toggle="tab"
                            v-on:click.prevent="getTable('Objectes', $event)">Objectes</span>
                    </li>
                    <li class="nav-item">
                        <span class="nav-link pointer hover-dark" data-toggle="tab"
                            v-on:click.prevent="getTable('Enemics', $event)">Enemics</span>
                    </li>
                    <li class="nav-item">
                        <span class="nav-link pointer hover-dark" data-toggle="tab"
                            v-on:click.prevent="getTable('Nivells', $event)">Nivells</span>
                    </li>
                    <li class="nav-item">
                        <span class="nav-link pointer hover-dark" data-toggle="tab"
                            v-on:click.prevent="getTable('Personatges', $event)">Personatges</span>
                    </li>
                    <li class="nav-item">
                        <span class="nav-link pointer hover-dark" data-toggle="tab"
                            v-on:click.prevent="getTable('StatsBase', $event)">Stats Base</span>
                    </li>
                    <li class="nav-item">
                        <span class="nav-link pointer hover-dark" data-toggle="tab"
                            v-on:click.prevent="getTable('StatsEnemics', $event)">Stats Enemics</span>
                    </li>
                    <li class="nav-item">
                        <span class="nav-link pointer hover-dark" data-toggle="tab"
                            v-on:click.prevent="getTable('Puntuacions', $event)">Puntuacions</span>
                    </li>
                </ul>
                <div class="tab-content col-md-11 col-sm-12 col-xs-12">
                    <div id="taulaobjectes">
                        <!-- Tabla Objetos -->
                        <template v-if="nameTableActive == 'Objectes'">
                            <table class="table table-hover">
                                <thead class="">
                                    <tr>
                                        <th>ID</th>
                                        <th>Descripció</th>
                                        <th>Ruta Imatge</th>
                                        <th>ID Stats</th>
                                        <th class="text-center w-100px">Modifica</th>
                                    </tr>
                                </thead>
                                <tbody class="">
                                    <tr v-for="item in itemsTableActive">
                                        <th scope="row">@{{item.idObjecte}}</th>
                                        <td>@{{item.Efecto}}</td>
                                        <td>@{{item.ImatgeRuta}}</td>
                                        <td>@{{item.idStats}}</td>
                                        <td class="text-right table-actions p-0 text-center text-justify align-middle">
                                            <a class="table-action hover-primary cursor-pointer" data-toggle="modal"
                                                v-on:click="edit(item.idObjecte)" data-target="#modal-center"><i
                                                    class="col-12 ti-settings"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </template>
                    </div>
                    <div id="taulaenemics">
                        <!-- Tabla Enemigos -->
                        <template v-if="nameTableActive == 'Enemics'">
                            <table class="table table-hover col-11">
                                <thead class="">
                                    <tr>
                                        <th>ID</th>
                                        <th>Nom</th>
                                        <th>Imatge Ruta</th>
                                        <th class="text-center w-100px">Modifica</th>
                                    </tr>
                                </thead>
                                <tbody class="">
                                    <tr v-for="item in itemsTableActive">
                                        <th scope="row">@{{item.idEnemic}}</th>
                                        <td>@{{item.Tipus}}</td>
                                        <td>@{{item.ImatgeRuta}}</td>
                                        <td class="text-right table-actions p-0 text-center text-justify align-middle">
                                            <a class="table-action hover-primary cursor-pointer" data-toggle="modal"
                                                v-on:click="edit(item.idEnemic)" data-target="#modal-center"><i
                                                    class="col-12 ti-settings"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </template>
                    </div>
                    <div id="taulanivells">
                        <!-- Tabla Niveles-->
                        <template v-if="nameTableActive == 'Nivells'">
                            <table class="table table-hover">
                                <thead class="">
                                    <tr>
                                        <th>ID</th>
                                        <th>Nom Nivell</th>
                                        <th>Imatge Nivell</th>
                                        <th class="text-center w-100px">Modifica</th>
                                    </tr>
                                </thead>
                                <tbody class="">
                                    <tr v-for="item in itemsTableActive">
                                        <th scope="row">@{{item.idNivell}}</th>
                                        <td>@{{item.NomNivell}}</td>
                                        <td>@{{item.ImatgeRuta}}</td>
                                        <td class="text-right table-actions p-0 text-center text-justify align-middle">
                                            <a class="table-action hover-primary cursor-pointer" data-toggle="modal"
                                                v-on:click="edit(item.idNivell)" data-target="#modal-center"><i
                                                    class="col-12 ti-settings"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </template>
                    </div>
                    <div id="taulapersonatges">
                        <template v-if="nameTableActive == 'Personatges'">
                            <table class="table table-hover">
                                <thead class="">
                                    <tr>
                                        <th>ID</th>
                                        <th>Nom Personatge</th>
                                        <th>Ruta Imatge</th>
                                        <th>ID Stats</th>
                                        <th class="text-center w-100px">Modifica</th>
                                    </tr>
                                </thead>
                                <tbody class="">
                                    <tr v-for="item in itemsTableActive">
                                        <th scope="row">@{{item.idPersonatge}}</th>
                                        <td>@{{item.Tipus}}</td>
                                        <td>@{{item.ImatgeRuta}}</td>
                                        <td>@{{item.idStats}}</td>
                                        <td class="text-right table-actions p-0 text-center text-justify align-middle">
                                            <a class="table-action hover-primary cursor-pointer" data-toggle="modal"
                                                v-on:click="edit(item.idPersonatge)" v- data-target="#modal-center">
                                                <i class="col-12 ti-settings"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </template>
                    </div>
                    <div id="taulastatsbase">
                        <template v-if="nameTableActive == 'StatsBase'">
                            <table class="table table-hover">
                                <thead class="">
                                    <tr>
                                        <th>ID</th>
                                        <th>HP</th>
                                        <th>DMG</th>
                                        <th>DEF</th>
                                        <th class="text-center w-100px">Modifica</th>
                                    </tr>
                                </thead>
                                <tbody class="">
                                    <tr v-for="item in itemsTableActive">
                                        <th scope="row">@{{item.idBase}}</th>
                                        <td>@{{item.HP}}</td>
                                        <td>@{{item.DMG}}</td>
                                        <td>@{{item.DEF}}</td>
                                        <td class="text-right table-actions p-0 text-center text-justify align-middle">
                                            <a class="table-action hover-primary cursor-pointer" data-toggle="modal"
                                                v-on:click="edit(item.idBase)" data-target="#modal-center"><i
                                                    class="col-12 ti-settings"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </template>
                    </div>
                    <div id="taulastatsenemics">
                        <template v-if="nameTableActive == 'StatsEnemics'">
                            <table class="table table-hover">
                                <thead class="">
                                    <tr>
                                        <th>ID</th>
                                        <th>ID Nivell</th>
                                        <th>HP</th>
                                        <th>DMG</th>
                                        <th>DEF</th>
                                        <th class="text-center w-100px">Modifica</th>
                                    </tr>
                                </thead>
                                <tbody class="">
                                    <tr v-for="item in itemsTableActive">
                                        <th scope="row">@{{item.idEnemic}}</th>
                                        <td>@{{item.idNivell}}</td>
                                        <td>@{{item.HP}}</td>
                                        <td>@{{item.DMG}}</td>
                                        <td>@{{item.DEF}}</td>
                                        <td class="text-right table-actions p-0 text-center text-justify align-middle">
                                            <a class="table-action hover-primary cursor-pointer" data-toggle="modal"
                                                v-on:click="edit(item.idEnemic)" data-target="#modal-center"><i
                                                    class="col-12 ti-settings"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </template>
                    </div>
                    <div id="taulapuntuacions">
                        <template v-if="nameTableActive == 'Puntuacions'">
                            <table class="table table-hover">
                                <thead class="">
                                    <tr>
                                        <th>ID</th>
                                        <th>Nom</th>
                                        <th>Puntuació</th>
                                        <th>ID Personatge</th>
                                        <th class="text-center w-100px">Modifica</th>
                                    </tr>
                                </thead>
                                <tbody class="">
                                    <tr v-for="item in itemsTableActive">
                                        <th scope="row">@{{item.idPuntuacio}}</th>
                                        <td>@{{item.Nom}}</td>
                                        <td>@{{item.Puntuacio}}</td>
                                        <td>@{{item.Tipus}}</td>
                                        <td class="text-right table-actions p-0 text-center text-justify align-middle">
                                            <a class="table-action hover-primary cursor-pointer" data-toggle="modal"
                                                v-on:click="edit(item.idPuntuacio)" data-target="#modal-center"><i
                                                    class="col-12 ti-settings"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </template>
                    </div>
                </div>
                <!-- Tabla 
                <table class="table table-hover col-11">
                    <thead class="">
                        <tr>
                            <th>ID</th>
                            <th>Nom</th>
                            <th>Imatge Ruta</th>
                            <th class="text-center w-100px">Modifica</th>
                        </tr>
                    </thead>
                    <tbody class="">

                        <tr>
                            <th scope="row">A</th>
                            <td>B</td>
                            <td>C</td>
                            <td class="text-right table-actions p-0 text-center text-justify align-middle">
                                <a class="table-action hover-primary cursor-pointer" data-toggle="modal"  data-target="#modal-center"><i class="col-12 ti-settings"></i></a>
                            </td>
                        </tr>

                    </tbody>
                </table> -->

                <!-- Modal -->
                <div class="modal modal-center fade" id="modal-center" tabindex="-1">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Edita registre</h5>
                                <button type="button" class="close hover-warning" data-dismiss="modal">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <template v-if="nameTableActive == 'Nivells'">
                                    <div class="form-group">
                                        <label for="nom">Nom de nivell</label>
                                        <input class="form-control" placeholder="Nom" type="text" name="nom"
                                            v-model="objecteEdicio.NomNivell">
                                        <label for="imatgeruta">Nom Imatge</label>
                                        <input class="form-control" placeholder="imatgeruta" type="text"
                                            name="imatgeruta" v-model="objecteEdicio.ImatgeRuta">
                                    </div>
                                </template>
                                <template v-if="nameTableActive == 'Enemics'">
                                    <div class="form-group">
                                        <label for="nom">Nom Enemic</label>
                                        <input class="form-control" placeholder="Nom" type="text" name="nom"
                                            v-model="objecteEdicio.Tipus">
                                        <label for="imatgeruta">Nom Imatge Enemic</label>
                                        <input class="form-control" placeholder="imatgeruta" type="text"
                                            name="imatgeruta" v-model="objecteEdicio.ImatgeRuta">
                                    </div>
                                </template>
                                <template v-if="nameTableActive == 'Puntuacions'">
                                    <div class="form-group">
                                        <label for="nom">Nom</label>
                                        <input class="form-control" placeholder="Nom" type="text" name="nom"
                                            v-model="objecteEdicio.Nom">
                                        <label for="puntuacio">Puntuació</label>
                                        <input class="form-control" placeholder="Puntuacio" type="text" name="puntuacio"
                                            v-model="objecteEdicio.Puntuacio">
                                        <label for="idpng">ID Personatge</label>
                                        <input class="form-control" placeholder="ID Personatge" type="text" name="idpng"
                                            v-model="objecteEdicio.idPNG">
                                    </div>
                                </template>
                                <template v-if="nameTableActive == 'StatsBase'">
                                    <div class="form-group">
                                        <label for="hp">HP</label>
                                        <input class="form-control" placeholder="HP" type="text" name="hp"
                                            v-model="objecteEdicio.HP">
                                        <label for="dmg">DMG</label>
                                        <input class="form-control" placeholder="DMG" type="text" name="dmg"
                                            v-model="objecteEdicio.DMG">
                                        <label for="def">DEF</label>
                                        <input class="form-control" placeholder="DEF" type="text" name="def"
                                            v-model="objecteEdicio.DEF">
                                    </div>
                                </template>
                                <template v-if="nameTableActive == 'StatsEnemics'">
                                    <div class="form-group">
                                        <label for="idn">ID Nivell</label>
                                        <input class="form-control" placeholder="ID Nivell" type="text" name="idn"
                                            v-model="objecteEdicio.idNivell">
                                        <label for="hp">HP</label>
                                        <input class="form-control" placeholder="HP" type="text" name="hp"
                                            v-model="objecteEdicio.HP">
                                        <label for="dmg">DMG</label>
                                        <input class="form-control" placeholder="DMG" type="text" name="dmg"
                                            v-model="objecteEdicio.DMG">
                                        <label for="def">DEF</label>
                                        <input class="form-control" placeholder="DEF" type="text" name="def"
                                            v-model="objecteEdicio.DEF">
                                    </div>
                                </template>
                                <template v-if="nameTableActive == 'Objectes'">
                                    <div class="form-group">
                                        <label for="efecto">Efecte</label>
                                        <input class="form-control" placeholder="Efecte" type="text" name="efecto"
                                            v-model="objecteEdicio.Efecto">
                                        <label for="imatgeruta">Nom Sprite</label>
                                        <input class="form-control" placeholder="Nom Sprite" type="text"
                                            name="imatgeruta" v-model="objecteEdicio.ImatgeRuta">
                                        <label for="idstats">ID Stats</label>
                                        <input class="form-control" placeholder="ID Stats" type="text" name="idstats"
                                            v-model="objecteEdicio.idStats">
                                    </div>
                                </template>
                                <template v-if="nameTableActive == 'Personatges'">
                                    <div class="form-group">
                                        <label for="nom">Nom Personatge</label>
                                        <input class="form-control" placeholder="Nom Personatge" type="text" name="nom"
                                            v-model="objecteEdicio.Tipus">
                                        <label for="imatgeruta">Nom Sprite</label>
                                        <input class="form-control" placeholder="Nom Sprite" type="text"
                                            name="imatgeruta" v-model="objecteEdicio.ImatgeRuta">
                                        <label for="ids">ID Stats</label>
                                        <input class="form-control" placeholder="ID Stats" type="text"
                                            name="ids" v-model="objecteEdicio.idStats">

                                    </div>
                                </template>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-bold btn-pure btn-warning hover-dark"
                                    data-dismiss="modal">Tanca</button>
                                <button type="button" @click.prevent="editobj($event)"
                                    class="btn btn-bold btn-pure btn-warning hover-dark">Guarda</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script src=" {{asset('js/backofficevue.js')}} "></script>
@endsection
