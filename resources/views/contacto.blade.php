@extends('layouts.nav')

@section('content')
<br>
<div class="text-center">
    <h1>Contacte</h1>
    <h3>Contacta amb nosaltres per conèixer més sobre aquest joc!</h3>
</div>
<div class="container">
    <div class="offset-lg-3 offset-md-3 offset-sm-1 col-md-6 col-xs-8 col-sm-8 row card-body my-3 bg-secondary py-60 offset-lg-2">
        <div class="flexbox">
            <img class="avatar avatar-xxl avatar-bordered" src="{{ asset('storage/imagenes/pau.jpeg') }}">
            <div class="flex-grow pl-20">
                <h4>Pau Sole Linares</h4>
                <div><i class="fas fa-phone w-20px"></i> +34 625 51 29 63</div>
                <div><i class="fa fa-fw fa-envelope w-20px"></i> pslau@gmail.com</div>
            </div>
        </div>
    </div>
    <div class="offset-lg-3 offset-md-3 offset-sm-1 col-md-6 col-xs-8 col-sm-8 row card-body my-3 bg-secondary py-60 offset-lg-2">
        <div class="flexbox">
            <img class="avatar avatar-xxl avatar-bordered" src="{{ asset('storage/imagenes/game.jpg') }}">
            <div class="flex-grow pl-20">
                <h4>Sebastian Sosa</h4>
                <div><i class="fas fa-phone w-20px"></i> +34 645 78 21 56</div>
                <div><i class="fa fa-fw fa-envelope w-20px"></i> sebasti@gmail.com</div>
            </div>
        </div>
    </div>
    

</div>

@endsection
