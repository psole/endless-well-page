@extends('layouts.nav')

@section('content')
<div class="mt-5">
    <h1 class="text-center">Frequently Asked Questions</h1>
    <div class="main-content">
        <div class="card">

            <div class="accordion accordion-connected" id="faq-general">
                <div class="card bg-dark card-inverse">
                    <h5 class="card-title bg-dark">
                        <a data-toggle="collapse" data-parent="#faq-general" href="#question-general-1"
                            aria-expanded="false" class="collapsed text-white">Que és aquest programa?</a>
                    </h5>

                    <div id="question-general-1" class="collapse" aria-expanded="false">
                        <div class="card-body pl-50">
                            Endless Well és un joc fet per Pau Solè i Sebastian Sosa.
                        </div>
                    </div>
                </div>


                <div class="card bg-dark card-inverse">
                    <h5 class="card-title bg-dark">
                        <a data-toggle="collapse" data-parent="#faq-general" href="#question-general-2"
                            class="collapsed text-white" aria-expanded="false">En quin llenguatge de programació està fet el joc?</a>
                    </h5>

                    <div id="question-general-2" class="collapse" aria-expanded="false">
                        <div class="card-body pl-50">
                            El joc està escrit amb el llenguatge de programació C# amb el motor de desenvolupament Unity.
                        </div>
                    </div>
                </div>


                <div class="card bg-dark card-inverse">
                    <h5 class="card-title bg-dark">
                        <a data-toggle="collapse" data-parent="#faq-general" href="#question-general-3"
                            class="collapsed text-white" aria-expanded="false">Per a quines plataformes es trobaran disponibles aquest videojoc?</a>
                    </h5>

                    <div id="question-general-3" class="collapse" aria-expanded="false">
                        <div class="card-body pl-50">
                            Principalment per a PC, a Windows i Linux.
                        </div>
                    </div>
                </div>

                <div class="card bg-dark card-inverse">
                    <h5 class="card-title bg-dark">
                        <a data-toggle="collapse" data-parent="#faq-general" href="#question-general-4"
                            class="collapsed text-white" aria-expanded="false">Què és la informació que es troba a la Wiki?</a>
                    </h5>

                    <div id="question-general-4" class="collapse" aria-expanded="false">
                        <div class="card-body pl-50">
                            El contingut dins la pàgina Wiki contè informació sobre els personatges, enemics, nivells i objectes dins els jocs.
                        </div>
                    </div>
                </div>

                <div class="card bg-dark card-inverse">
                    <h5 class="card-title bg-dark">
                        <a data-toggle="collapse" data-parent="#faq-general" href="#question-general-5"
                            class="collapsed text-white" aria-expanded="false">És gratuït aquest joc?</a>
                    </h5>

                    <div id="question-general-5" class="collapse" aria-expanded="false">
                        <div class="card-body pl-50">
                            Si. La descàrrega d'aquest joc és totalment gratuïta.
                        </div>
                    </div>
                </div>

                <div class="card bg-dark card-inverse">
                    <h5 class="card-title bg-dark">
                        <a data-toggle="collapse" data-parent="#faq-general" href="#question-general-6"
                            class="collapsed text-white" aria-expanded="false">Aquesta és la versió final del joc?</a>
                    </h5>

                    <div id="question-general-6" class="collapse" aria-expanded="false">
                        <div class="card-body pl-50">
                            No. Volem treballar en el joc actualitzant-lo contínuament per oferir un joc més complet amb cada actualització.
                        </div>
                    </div>
                </div>

                <div class="card bg-dark card-inverse">
                    <h5 class="card-title bg-dark">
                        <a data-toggle="collapse" data-parent="#faq-general" href="#question-general-7"
                            class="collapsed text-white" aria-expanded="false">Com puc contactar amb els creadors?</a>
                    </h5>

                    <div id="question-general-7" class="collapse" aria-expanded="false">
                        <div class="card-body pl-50">
                            Pots contactar-nos amb la informació facilitada a <a class="hover-white text-warning" href="{{url('/contacto')}}">aquesta pàgina</a>.
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!--
<div class="container spacing">
    <div class="text-center">
        <h1 class="display-4"> <i class="far fa-lightbulb"></i> Preguntes Freqüents</h1>
    </div>
    <div id="accordion">
        <div class="card backgroundNav faqQuestion">
            <div type="button" class="card-header btn text-left pregunta" id="headingOne" data-toggle="collapse"
                data-target="#collapseOne">
                <h5 class="mb-0">
                    Pregunta 1
                </h5>
            </div>

            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                    3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                    laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin
                    coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes
                    anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings
                    occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard
                    of them accusamus labore sustainable VHS.
                </div>
            </div>
        </div>
        <div class="card backgroundNav faqQuestion">
            <div type="button" class="card-header btn text-left pregunta" id="headingTwo" data-toggle="collapse"
                data-target="#collapseTwo">
                <h5 class="mb-0">
                    Pregunta 2
                </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                    3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                    laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin
                    coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes
                    anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings
                    occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard
                    of them accusamus labore sustainable VHS.
                </div>
            </div>
        </div>
        <div class="card backgroundNav faqQuestion">
            <div type="button" class="card-header btn text-left pregunta" id="headingThree" data-toggle="collapse"
                data-target="#collapseThree">
                <h5 class="mb-0">
                    Pregunta 3
                </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                    3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                    laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin
                    coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes
                    anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings
                    occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard
                    of them accusamus labore sustainable VHS.
                </div>
            </div>
        </div>
        <div class="card backgroundNav faqQuestion">
            <div type="button" class="card-header btn text-left pregunta" id="headingThree" data-toggle="collapse"
                data-target="#collapseFourth">
                <h5 class="mb-0">
                    Pregunta 4
                </h5>
            </div>
            <div id="collapseFourth" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                    3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                    laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin
                    coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes
                    anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings
                    occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard
                    of them accusamus labore sustainable VHS.
                </div>
            </div>
        </div>
        <div class="card backgroundNav faqQuestion">
            <div type="button" class="card-header btn text-left pregunta" id="headingThree" data-toggle="collapse"
                data-target="#collapseFive">
                <h5 class="mb-0">
                    Pregunta 5
                </h5>
            </div>
            <div id="collapseFive" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                    3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                    laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin
                    coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes
                    anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings
                    occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard
                    of them accusamus labore sustainable VHS.
                </div>
            </div>
        </div>
        <div class="card backgroundNav faqQuestion">
            <div type="button" class="card-header btn text-left pregunta" id="headingThree" data-toggle="collapse"
                data-target="#collapseSix">
                <h5 class="mb-0">
                    Pregunta 6
                </h5>
            </div>
            <div id="collapseSix" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                    3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                    laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin
                    coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes
                    anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings
                    occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard
                    of them accusamus labore sustainable VHS.
                </div>
            </div>
        </div>
    </div>
</div>
-->
@endsection
