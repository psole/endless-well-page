@extends('layouts.nav')

@section('content')   
    <header class="row spacing">
        <div class="col-12">
            <img class="mx-auto d-block" src="{{asset('storage/imagenes/waterwell.png')}}">
        </div>
        <div class="col-12">
            <h1 class="text-center">Endless Well</h1>
        </div>
    </header>
    <section class="nav nav-pills flex-column flex-sm-row">
        <a class="flex-fill text-center nav-link" href="{{url('/descarga')}}">
            <i class="fas fa-download"></i>
            <h3>Descarga</h3>
        </a>
        <a class="flex-fill text-center nav-link" href="{{url('/faq')}}">
            <i class="fas fa-question-circle"></i>
            <h3>FAQ</h3>
        </a>
        <a class="flex-fill text-center nav-link" href="{{url('/wiki')}}">
            <i class="fab fa-wikipedia-w"></i>
            <h3>Wiki</h3>
        </a>
        <a class="flex-fill text-center nav-link" href="{{url('/contacto')}}">
            <i class="fas fa-id-card"></i>
            <h3>Contacte</h3>
        </a>
    </section>
    <section class="spacing">
        <p class="col-md-12 text-center">
        Benvinguts a la pàgina de "Endless Well", un joc fet per Pau Solè Linares i Sebastian Sosa.<br>
        Explora les opcions de la pàgina per obtenir més informació del joc o dels creadors.
        </p>
        <p class="col-md-12 text-center">
        </p>
    </section>

@endsection