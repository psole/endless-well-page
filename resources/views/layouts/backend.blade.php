<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Projecte</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.0.min.js"
        integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg=" crossorigin="anonymous"></script>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,300i" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <link rel="stylesheet" href="{{asset('asset/css/core.min.css')}}">
    <link rel="stylesheet" href="{{asset('asset/css/app.min.css')}}">
    <link rel="stylesheet" href="{{asset('asset/css/style.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/inici.css')}}" />
    <link rel="stylesheet" href="{{asset('css/faq.css')}}" />
    <script>
        var token = '{{csrf_token()}}'; 
    </script>
</head>
<body>
    <br><br>
    <header class="topbar topbar-expand-xl">
        <div class="topbar-left">
            <span class="topbar-btn topbar-menu-toggler"><i>&#9776;</i></span>
            <span class="topbar-brand"><img src="{{asset('storage/imagenes/waterwell.png')}}" alt="logo"
                    style="height: 30px;width: auto;"></span>
            <div class="topbar-divider d-none d-xl-block"></div>
            <nav class="topbar-navigation">
                <ul class="menu">
                    <li class="menu-item menu-warning">
                        <a class="menu-link" href="{{url('/inici')}}">
                            <span class="title">Torna a Inici</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>


        <div class="topbar-right">
            <ul class="topbar-btns">
                @guest
                <li class="dropdown">
                    <a class="nav-link" href="{{ route('login') }}">
                        <span class="topbar-btn"><i class=" avatar far fa-user"></i></span>
                    </a>
                </li>
                @else
                <li class="dropdown">
                    @if (Auth::user()->name == 'Sebas')
                        <span class="topbar-btn" data-toggle="dropdown">
                            <img class="avatar avatar-sm" src="{{ asset('storage/imagenes/game.jpg') }}">
                        </span>    
                    @elseif (Auth::user()->name == 'Pau')
                        <span class="topbar-btn" data-toggle="dropdown">
                            <img class="avatar avatar-sm" src="{{ asset('storage/imagenes/pau.jpeg') }}">
                        </span>
                    @else
                        <span class="topbar-btn" data-toggle="dropdown">
                            <i class="avatar fas fa-user-cog"></i>
                        </span>
                        <!--
                        <span class="topbar-btn" data-toggle="dropdown">
                            <img class="avatar avatar-sm" src="{{ asset('storage/imagenes/pau.jpeg') }}">
                        </span>
                        -->
                    @endif 
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="{{url('/backoffice')}}"><i class="ti-user"></i>
                            Backoffice</a>
                        <a class="dropdown-item" href="#"><i class="ti-settings"></i> Settings</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i
                                class="ti-power-off"></i> Logout
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </a>
                    </div>
                </li>
                @endguest

            </ul>

            <div class="topbar-divider d-none d-md-block"></div>
        </div>
    </header>
    @yield('content')

    <script src="{{asset('asset/js/core.min.js')}}"></script>
    <script src="{{asset('asset/js/app.min.js')}}"></script>
    <script src="{{asset('asset/js/script.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
</body>

</html>
