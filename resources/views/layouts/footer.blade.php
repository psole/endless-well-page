<footer class="site-footer pt-50 pb-20">
    <div class="row gap-y">
        <div class="col-lg-5">
            <h5 class="text-uppercase fs-14 ls-1">About</h5>
            <p class="text-justify"><strong>TheAdmin</strong> is a powerful, responsive, and high-performance admin template. It's based on Bootstrap and contains a lot of components to easily make an admin, dashboard. This template comes with a simple yet beautiful design which focused on ease of use for users. Efficiently expedite equity invested products rather than extensive outsourcing. Holisticly deliver principle centered imperatives after.</p>
        </div>

        <div class="col-6 col-md-4 col-lg-2 text-left1 text-lg-center1">
            <h5 class="text-uppercase fs-14 ls-1">Company</h5>
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="#">About us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">How it works</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Terms of use</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Privacy policy</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact us</a>
                </li>
            </ul>
        </div>

        <div class="col-6 col-md-4 col-lg-2 text-left1 text-lg-center1">
            <h5 class="text-uppercase fs-14 ls-1">Support</h5>
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="#">Help center</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Tutorials</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Forums</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Official blog</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Ask question</a>
                </li>
            </ul>
        </div>

        <div class="col-md-4 col-lg-3 text-left1 text-lg-center1">
            <h5 class="text-uppercase fs-14 ls-1">Newsletter</h5>
            <p><strong>Subscribe</strong> to our newsletter to receive news, updates, and special offers:</p>
            <br>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Enter your email address">
            </div>
            <button class="btn btn-primary btn-block" type="button"><i class="fa fa-paper-plane"></i> Subscribe</button>
        </div>

    </div>

    <hr>

    <div class="row">
        <div class="col-md-6 justify-content-center justify-content-md-start">
            <p>Copyright © 2019 <a href="http://thetheme.io/theadmin">TheAdmin</a>. All rights reserved.</p>
        </div>

        <div class="col-md-6">
            <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
                <li class="nav-item">
                    <a class="nav-link" href="#">Terms of use</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Privacy policy</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">About</a>
                </li>
            </ul>
        </div>
    </div>
</footer>
<script src="{{asset('asset/js/core.min.js')}}"></script>
<script src="{{asset('asset/js/app.min.js')}}"></script>
<script src="{{asset('asset/js/script.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>
