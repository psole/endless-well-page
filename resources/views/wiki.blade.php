@extends('layouts.nav')

@section('content')
<br>
<br>
<div id="wiki">
    <h1 class="text-center">WIKI</h1>
    <div v-if="objEntrada" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="col-md-6 col-lg-4 mx-auto">
            <div class="card bg-dark card-inverse p-30 pt-50 text-center">
                <div v-if="entrada == 'Enemic'">
                    <div>
                    <h1>@{{entrada}}</h1>
                        <a class="avatar avatar-xxl mb-3" href="#">
                        <img onerror="javascript:wikiVue.objEntrada.ImatgeRuta = 'unavailable.png'" v-bind:src="urlimg + objEntrada.ImatgeRuta"  >
                        </a>
                    </div>
                    <h5><a class="text-white hover-warning" href="#">@{{objEntrada.Tipus}}</a></h5>
                    <p class="fs-13">Dades</p>
                    <!--<p class="text-white fs-12 mb-50">@{{objEntrada}}</p>-->
                    <div class="media">
                        <p class="fs-18 d-block col-6 fw-400">HP</p>
                        <p class="fs-18 d-block col-6 fw-400">@{{objEntrada.HP}}</p>
                    </div>
                    <div class="media">
                        <p class="fs-18 d-block col-6 fw-400">DMG</p>
                        <p class="fs-18 d-block col-6 fw-400">@{{objEntrada.DMG}}</p>
                    </div>
                    <div class="media">
                            <p class="fs-18 d-block col-6 fw-400">DEF</p>
                            <p class="fs-18 d-block col-6 fw-400">@{{objEntrada.DEF}}</p>
                    </div>
                </div>
                <div v-if="entrada == 'Personatge'">
                    <div>
                        <h1>@{{entrada}}</h1>
                        <a class="avatar avatar-xxl mb-3" href="#">
                            <img onerror="javascript:wikiVue.objEntrada.ImatgeRuta = 'unavailable.png'" v-bind:src="urlimg + objEntrada.ImatgeRuta">
                        </a>
                    </div>
                    <h5><a class="text-white hover-warning" href="#">@{{objEntrada.Tipus}}</a></h5>
                    <p class="fs-13">Dades</p>
                    <!--<p class="text-white fs-12 mb-50">@{{objEntrada}}</p>-->
                    <div class="media">
                        <p class="fs-18 d-block col-6 fw-400">HP</p>
                        <p class="fs-18 d-block col-6 fw-400">@{{objEntrada.HP}}</p>
                    </div>
                    <div class="media">
                        <p class="fs-18 d-block col-6 fw-400">DMG</p>
                        <p class="fs-18 d-block col-6 fw-400">@{{objEntrada.DMG}}</p>
                    </div>
                    <div class="media">
                            <p class="fs-18 d-block col-6 fw-400">DEF</p>
                            <p class="fs-18 d-block col-6 fw-400">@{{objEntrada.DEF}}</p>
                    </div>
                </div>
                <div v-if="entrada == 'Objectes'">
                    <div>
                        <h1>Objecte</h1>
                        <a class="avatar avatar-xxl mb-3" href="#">
                            <img onerror="javascript:wikiVue.objEntrada.ImatgeRuta = 'unavailable.png'" v-bind:src="urlimg + objEntrada.ImatgeRuta">
                        </a>
                    </div>
                    <h5><a class="text-white hover-warning" href="#">@{{objEntrada.Efecto}}</a></h5>
                    <p class="fs-13">Dades</p>
                    <!--<p class="text-white fs-12 mb-50">@{{objEntrada}}</p>-->
                    <div class="media">
                        <p class="fs-18 d-block col-6 fw-400">HP</p>
                        <p class="fs-18 d-block col-6 fw-400">+ @{{objEntrada.HP}}</p>
                    </div>
                    <div class="media">
                        <p class="fs-18 d-block col-6 fw-400">DMG</p>
                        <p class="fs-18 d-block col-6 fw-400">+ @{{objEntrada.DMG}}</p>
                    </div>
                    <div class="media">
                            <p class="fs-18 d-block col-6 fw-400">DEF</p>
                            <p class="fs-18 d-block col-6 fw-400">+ @{{objEntrada.DEF}}</p>
                    </div>
                </div>
                <div v-if="entrada == 'Nivell'">
                    <div>
                        <h1>@{{entrada}}</h1>
                        <a class="avatar avatar-xxl mb-3" href="#">
                            <img onerror="javascript:wikiVue.objEntrada.ImatgeRuta = 'unavailable.png'" v-bind:src="urlimg + objEntrada.ImatgeRuta" >
                        </a>
                    </div>
                    <h5><a class="text-white hover-warning" href="#">@{{objEntrada.NomNivell}}</a></h5>
                </div>
            </div>
        </div>
    </div>
    <div
        class="col-xs-12 col-sm-12 col-md-12 col-lg-12 flexbox flex-wrap align-content-start justify-content-center flex-wrap">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
            <div class="card bg-dark card-inverse">
                <header class="card-header">
                    <h4 class="card-title">Enemics</h4>
                </header>

                <div class="card-content scrollable ps-container ps-theme-default ps-active-y" style="height: 190px;"
                    data-ps-id="a997ce94-4578-9d3c-53a0-3582e483572f">
                    <div class="card-body px-0 py-0">
                        <div class="col-12 px-0 py-0 media-list media-list-hover">
                            @foreach ($enemics as $item)
                            <div class="media hover-warning items-center cursor-pointer"
                                v-on:click="getEntrada('en_{{$item->idEnemic}}', $event)">
                                <p class="fs-18 text-center fw-400">{{$item->Tipus}}</p>
                            </div>
                            @endforeach

                        </div>
                    </div>
                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;">
                        <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps-scrollbar-y-rail" style="top: 0px; height: 190px; right: 2px;">
                        <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 71px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
            <div class="card bg-dark card-inverse">
                <header class="card-header">
                    <h4 class="card-title">Personatges</h4>
                </header>

                <div class="card-content scrollable ps-container ps-theme-default ps-active-y" style="height: 190px;"
                    data-ps-id="a997ce94-4578-9d3c-53a0-3582e483572f">
                    <div class="card-body px-0 py-0">
                        <div class="col-12 px-0 py-0 media-list media-list-hover">
                            @foreach ($personatges as $item)
                            <div class="media hover-warning items-center cursor-pointer"
                                v-on:click="getEntrada('pe_{{$item->idPersonatge}}', $event)">
                                <p class="fs-18 text-center fw-400">{{$item->Tipus}}
                                </p>
                            </div>
                            @endforeach

                        </div>
                    </div>
                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;">
                        <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps-scrollbar-y-rail" style="top: 0px; height: 190px; right: 2px;">
                        <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 71px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
            <div class="card bg-dark card-inverse">
                <header class="card-header">
                    <h4 class="card-title">Objectes</h4>
                </header>

                <div class="card-content scrollable ps-container ps-theme-default ps-active-y" style="height: 190px;"
                    data-ps-id="a997ce94-4578-9d3c-53a0-3582e483572f">
                    <div class="card-body px-0 py-0">
                        <div class="col-12 px-0 py-0 media-list media-list-hover">
                            @foreach ($objectes as $item)
                            <div class="media hover-warning items-center cursor-pointer"
                                v-on:click="getEntrada('ob_{{$item->idObjecte}}', $event)">
                                <p class="fs-18 text-center fw-400">{{$item->Efecto}}
                                </p>
                            </div>
                            @endforeach

                        </div>
                    </div>
                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;">
                        <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps-scrollbar-y-rail" style="top: 0px; height: 190px; right: 2px;">
                        <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 71px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
            <div class="card bg-dark card-inverse">
                <header class="card-header">
                    <h4 class="card-title">Nivells</h4>
                </header>

                <div class="card-content scrollable ps-container ps-theme-default ps-active-y" style="height: 190px;"
                    data-ps-id="a997ce94-4578-9d3c-53a0-3582e483572f">
                    <div class="card-body px-0 py-0">
                        <div class="col-12 px-0 py-0 media-list media-list-hover">
                            @foreach ($nivells as $item)
                            <div class="media hover-warning items-center cursor-pointer"
                                v-on:click="getEntrada('ni_{{$item->idNivell}}', $event)">
                                <p class="fs-18 text-center fw-400">{{$item->NomNivell}}
                                </p>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;">
                        <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps-scrollbar-y-rail" style="top: 0px; height: 190px; right: 2px;">
                        <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 71px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src=" {{asset('js/wikivue.js')}} "></script>

@endsection
