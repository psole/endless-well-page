<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('inici');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/inici', 'ProjecteController@inici');
Route::get('/descarga', 'ProjecteController@descarga');
Route::get('/faq', 'ProjecteController@faq');
Route::get('/wiki', 'WikiController@index');
Route::get('/contacto', 'ProjecteController@contacto');
/*
Route::get('/backoffice', function () {
    return redirect()->route('objectes');
});
*/
/**
 * Backoffice
 */
Route::get('/backoffice', 'BackofficeController@index')->middleware('auth');

//GET OBJETOS WIKI
Route::post('/wiki/getentrada', 'WikiController@getEntrada');
//EDICIO BACKOFFICE
Route::get('/backoffice/{taula}/edit/{id}', 'BackofficeController@edit');
Route::post('/backoffice/editobj', 'BackofficeController@editobj');
//OBJECTES
Route::get('/backoffice/getObjectes', 'BackofficeController@getObjectes');
//NIVELLS
Route::get('/backoffice/getNivells', 'BackofficeController@getNivells');
//ENEMICS
Route::get('/backoffice/getEnemics', 'BackofficeController@getEnemics');
//PERSONATGES
Route::get('/backoffice/getPersonatges', 'BackofficeController@getPersonatges');
//PUNTUACIO
Route::get('/backoffice/getPuntuacions', 'BackofficeController@getPuntuacions');
//STATSBASE
Route::get('/backoffice/getStatsBase', 'BackofficeController@getStatsBase');
//STATSENEMICS
Route::get('/backoffice/getStatsEnemics', 'BackofficeController@getStatsEnemics');
